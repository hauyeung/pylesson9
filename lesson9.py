'''
Created on Jun 10, 2014

@author: hauyeung
'''
from tkinter import *

root = Tk()
root.wm_title("Frame Layout")
root.grid()
colors = ['Red','Blue','Green','Black','Open']
buttons = []
for r in range(6):
    root.rowconfigure(r, weight=1)
    
for c in range(5):
    root.columnconfigure(c, weight=1)
    b = Button(root, text=colors[c])
    b.grid(row = 6, column = c, sticky = E+W)
    buttons.append(b)
frame1 = Frame(root,bg='orange',name='frame 1', width=80)
frame1.grid(row = 0, column = 0, rowspan = 3, columnspan = 2, sticky = W+E+N+S)
frame2 = Frame(root,bg='white',name='frame 2', width=80)
frame2.grid(row = 3, column = 0, rowspan = 3, columnspan = 2, sticky = W+E+N+S)
frame3 = Frame(root,bg='yellow')
frame3.grid(row = 0, column = 2, rowspan = 6, columnspan = 3, sticky = W+E+N+S)
frame3.columnconfigure(0,weight=1)
frame3.rowconfigure(0,weight=1)
l = Text(frame3,width=0)
l.grid(row=0, column=0, rowspan=6, columnspan=3, sticky =W+E+N+S)
e = Entry(frame3)
e.grid(row=3, column = 0, rowspan = 6, columnspan=3,sticky=W+E+N+S)

def openfile(event):
    try:
        file = open(e.get(),'r')
        l.insert(INSERT, file.read())
    except Exception:
        pass
    
def changecolor(event):
    l.tag_add('all','1.0','400.0')
    l.tag_config('all',foreground = event.widget.cget('text'))
    
def getcoord(event):
    print (str(event.x)+", "+ str(event.y)+" - "+str(event.widget))

buttons[0].bind("<Button-1>", changecolor)
buttons[1].bind('<Button-1>', changecolor)
buttons[2].bind('<Button-1>', changecolor)
buttons[3].bind('<Button-1>', changecolor)
buttons[4].bind('<Button-1>', openfile)
frame1.bind('<Button-1>', getcoord)
frame2.bind('<Button-1>', getcoord)


root.mainloop()